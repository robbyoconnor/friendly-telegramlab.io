---
title: Configuration
layout: default
parent: Web
---

# {{ page.title }}

1. Follow the instructions in [the docs](../auth) to log in
2. Click the three bars at the top left, and choose `Config`
3. There is a list of configuration entries, along with their short help descriptions and default values. Scroll to the entry you wanted to edit.
4. Type the new value into the textbox. The value is auto-saved when you exit the textbox.
